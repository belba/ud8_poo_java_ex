import dto.Persona;

public class PersonaApp {

	public static void main(String[] args) {
		// TODO Auto-generated method stub

		Persona p1 = new Persona("Yassine",25,'H');
		
		System.out.println("Nombre: "+p1.getNombre());
		System.out.println("Edad: "+p1.getEdad());
		System.out.println("Sexo "+ ((p1.getSexo() == 'H') ? "Hombre" : "Mujer"));
	}

}
