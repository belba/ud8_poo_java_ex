package dao;

public class Serie {
	
	// Atributos
	private String titulo;
	private int numeroTemporadas;
	private boolean entregado;
	private String genero;
	private String creador;
	
	// valores constantes
	private static final int numeroTemporadas3 = 3;
	private static final boolean entregadoFalse = false;
	
	// Constructores
	public Serie() {
		this.titulo = "Juego de tronos";
		this.numeroTemporadas = numeroTemporadas3;
		this.entregado = entregadoFalse;
		this.genero = "aventura";
		this.creador = "D. B. Weiss";
	}
	public Serie(String titulo, String creador) {
		super();
		this.titulo = titulo;
		this.creador = creador;
	}
	
	public Serie(String titulo, int numeroTemporadas, boolean entregado, String genero, String creador) {
		super();
		this.titulo = titulo;
		this.numeroTemporadas = numeroTemporadas;
		this.entregado = entregado;
		this.genero = genero;
		this.creador = creador;
	}
	
	//Getters
	
	/**
	 * @return the titulo
	 */
	public String getTitulo() {
		return titulo;
	}
	/**
	 * @return the numeroTemporadas
	 */
	public int getNumeroTemporadas() {
		return numeroTemporadas;
	}
	/**
	 * @return the entregado
	 */
	public boolean isEntregado() {
		return entregado;
	}
	/**
	 * @return the genero
	 */
	public String getGenero() {
		return genero;
	}
	/**
	 * @return the creador
	 */
	public String getCreador() {
		return creador;
	}
	/**
	 * @return the numerotemporadas3
	 */
	public static int getNumerotemporadas3() {
		return numeroTemporadas3;
	}
	
	
	// Setters
	
	/**
	 * @return the entregadofalse
	 */
	public static boolean isEntregadofalse() {
		return entregadoFalse;
	}
	/**
	 * @param titulo the titulo to set
	 */
	public void setTitulo(String titulo) {
		this.titulo = titulo;
	}
	/**
	 * @param numeroTemporadas the numeroTemporadas to set
	 */
	public void setNumeroTemporadas(int numeroTemporadas) {
		this.numeroTemporadas = numeroTemporadas;
	}
	/**
	 * @param entregado the entregado to set
	 */
	public void setEntregado(boolean entregado) {
		this.entregado = entregado;
	}
	/**
	 * @param genero the genero to set
	 */
	public void setGenero(String genero) {
		this.genero = genero;
	}
	/**
	 * @param creador the creador to set
	 */
	public void setCreador(String creador) {
		this.creador = creador;
	}
	
	
	@Override
	public String toString() {
		return "Serie [titulo=" + titulo + ", numeroTemporadas=" + numeroTemporadas + ", entregado=" + entregado
				+ ", genero=" + genero + ", creador=" + creador + "]";
	}
	
	

}
