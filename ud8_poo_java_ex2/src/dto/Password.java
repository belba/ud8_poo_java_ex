package dto;

public class Password {
	
	private int longitud;
	private String contraseña;
	
	
	public Password() {
		this.longitud = 8;
	
	}

	public Password(int longitud) {
		this.longitud = longitud;
	}

	//Getters	
	
	/**
	 * @return the longitud
	 */
	public int getLongitud() {
		return longitud;
	}

	/**
	 * @return the contraseña
	 */
	public String getContraseña() {		
		return String.valueOf(generarContraseña(contraseña));
	}

	// Settings
	
	/**
	 * @param longitud the longitud to set
	 */
	public void setLongitud(int longitud) {
		this.longitud = longitud;
	}

	/**
	 * @param contraseña the contraseña to set
	 */
	public void setContraseña(String contraseña) {
		this.contraseña = contraseña;
	}
	
	
	private int generarContraseña(String contraseña2) { // Metodo para generar una contraseña aleatoria
		int contraGenerada = (int) Math.floor(Math.random()*this.longitud+1);		
		return contraGenerada;
	}

	/**
	 * @param longitud
	 * @param contraseña
	 */
	public Password(int longitud, String contraseña) {
		super();
		this.longitud = longitud;
		this.contraseña = contraseña;
	}
	
	
}
