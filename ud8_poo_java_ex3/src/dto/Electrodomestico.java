package dto;

public class Electrodomestico {
	
	// Atributos
	private double precioBase;
	private String color;
	private char consumoEnergetico;
	private int peso;
	
	
	// valores constantes
	private static final String colorBanco = "blanco";
	private static final char consumoEnergeticoF = 'F';
	private static final double preciobase100 = 100;
	private static final int peso5 = 5;

	public Electrodomestico() {    
		this.color = colorBanco;
		this.consumoEnergetico = consumoEnergeticoF;
		this.precioBase = preciobase100;
		this.peso = peso5;
	}

	public Electrodomestico(double precioBase, int peso) {
		this.precioBase = precioBase;
		this.peso = peso;
	}

	public Electrodomestico(double precioBase, String color, char consumoEnergetico, int peso) {
		this.precioBase = precioBase;
		this.color = color;
		this.consumoEnergetico = consumoEnergetico;
		this.peso = peso;
	}

	// Getters
	
	/**
	 * @return the precioBase
	 */
	public double getPrecioBase() {
		return precioBase;
	}

	/**
	 * @return the color
	 */
	public String getColor() {
		if(color.equalsIgnoreCase("rojo") && 
				color.equalsIgnoreCase("blanco") &&
				color.equalsIgnoreCase("negro") &&
				color.equalsIgnoreCase("azul") &&
				color.equalsIgnoreCase("gris")) {
			return color;
		}else {
			return colorBanco;			
		}	
	}

	/**
	 * @return the consumoEnergetico
	 */
	public char getConsumoEnergetico() {
		return consumoEnergetico;
	}

	/**
	 * @return the peso
	 */
	public int getPeso() {
		
		return peso;
	}

	//Settings
	
	/**
	 * @param precioBase the precioBase to set
	 */
	public void setPrecioBase(double precioBase) {
		this.precioBase = precioBase;
	}

	/**
	 * @param color the color to set
	 */
	public void setColor(String color) {

		this.color = color;
		
	}

	/**
	 * @param consumoEnergetico the consumoEnergetico to set
	 */
	public void setConsumoEnergetico(char consumoEnergetico) {
		this.consumoEnergetico = consumoEnergetico;
	}

	/**
	 * @param peso the peso to set
	 */
	public void setPeso(int peso) {
		this.peso = peso;
	}

	@Override
	public String toString() {
		return "Electrodomestico [precioBase=" + precioBase + ", color=" + color + ", consumoEnergetico="
				+ consumoEnergetico + ", peso=" + peso + "]";
	}
	
	
	
	
}
