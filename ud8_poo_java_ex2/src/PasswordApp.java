import java.util.Scanner;

import dto.Password;

public class PasswordApp {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		
		Scanner longitudContra = new Scanner(System.in);
		System.out.print("Introduce la longitud de la contraseña: ");	
		
		int longitudContraIntro = longitudContra.nextInt();
		Password pass = new Password();
		Password pass1 = new Password(longitudContraIntro);
		
		//int generarPassword = (int) Math.floor(Math.random()*pass1.getLongitud()+1);
		
		//pass1.setContraseña(String.valueOf(generarPassword));
		
		System.out.println("Contraseña generada con una longitud introducida por pantalla: "+pass1.getContraseña());
		System.out.println("Contraseña generada por defecto: "+pass.getContraseña());
	}

}
